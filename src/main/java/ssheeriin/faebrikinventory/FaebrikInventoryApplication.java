package ssheeriin.faebrikinventory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication 
@EnableConfigServer
@EnableEurekaClient
public class FaebrikInventoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(FaebrikInventoryApplication.class, args);
	}

}

