package ssheeriin.faebrikinventory.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Item
 */
@Getter 
@Setter 
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class Item {

    private Long itemCode;
    private String description;
    private double basePrice;
    private Category category;

}