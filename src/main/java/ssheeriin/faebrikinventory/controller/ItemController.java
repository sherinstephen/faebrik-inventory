package ssheeriin.faebrikinventory.controller;

import javax.ws.rs.PathParam;

import com.netflix.discovery.EurekaClient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ssheeriin.faebrikinventory.model.Category;
import ssheeriin.faebrikinventory.model.Item;

/**
 * ItemController
 */
@RestController
public class ItemController {

    @Autowired
    @Lazy
    private EurekaClient eurekaClient;

    @RequestMapping("/item/{code}")
    public Item findItem(@PathParam("code") String code) {
        return new Item(1L, "desc", 10.0, new Category("cat1", "cat1 desc"));
    }

}